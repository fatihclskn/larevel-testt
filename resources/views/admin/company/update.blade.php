@extends('admin.layouts.master')
@section('title','Müşteri güncelleme')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Şirket Güncelle</h6>
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <form action="{{ route('company.update',$company->id) }}" method="POST" enctype="multipart/form-data" >
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="">Şirket İsmi</label>
                    <input type="text" class="form-control" name="company_name" value="{{ $company->company_name }}">
                </div>
                <div class="form-group">
                    <label for="">İsim Soyisim</label>
                    <input type="text" class="form-control" name="name" value="{{ $company->name_surname }}">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control" name="email" value="{{ $company->email }}">
                </div>
                <div class="form-group">
                    <label for="">WebSite</label>
                    <input type="text" class="form-control" name="website" value="{{ $company->website }}">
                </div>

                <div class="form-group">
                    <label >Müşteri</label>
                    <select name="customer" class="form-control">
                        <option value="">Secim Yapınız</option>
                        @foreach($customers as $customer)
                            <option value="{{ $customer->id }}">{{ $customer->name_surname }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="form-control btn btn-outline-primary">Bilgileri Güncelle</button>
                </div>
            </form>
        </div>
    </div>



@endsection
