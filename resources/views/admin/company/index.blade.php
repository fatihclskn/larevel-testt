@extends('admin.layouts.master')
@section('title','Hizmet Veren Şirketler')
@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tüm Şirketler</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Logo</th>
                        <th>Firma Adı</th>
                        <th>İsim Soyisim</th>
                        <th>E-mail</th>
                        <th>WebSite</th>
                        <th>Müsteriler</th>
                        <th>Olusturma Tarihi</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <td>{{ $company->id }}</td>
                            <td>
                                <img src="{{ $company->logo }}" alt="" width="90">
                            </td>
                            <td>{{ $company->company_name }}</td>
                            <td>{{ $company->name_surname}}</td>
                            <td>{{ $company->email }}</td>
                            <td>{{ $company->website }}</td>
                            <td>{{ $company->getCustomer->name_surname }}</td>
                           <td>{{ \Carbon\Carbon::parse($company->created_at)->format('j F, Y') }}</td>
                            <td >
                                <a href="#" class="btn btn-outline-success form-control mb-1"><i class="fa fa-eye"></i></a>
                                <a href="{{ route('company.edit',$company->id) }}"   class="btn btn-outline-secondary form-control mb-1"><i class="fa fa-edit"></i></a>
                                <form action="{{ route('company.destroy',$company->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button href="" class="btn btn-outline-danger form-control"><i class="fa fa-times"></i></button>

                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>

            </div>
        </div>
    </div>


@endsection
