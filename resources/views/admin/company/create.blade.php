@extends('admin.layouts.master')
@section('title','Şirket Olustur')
@section('content')


    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Yeni Şirket Kaydı</h6>
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <form action="{{ route('company.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="">Şirket İsmi</label>
                    <input type="text" class="form-control" name="company_name">
                </div>
                <div class="form-group">
                    <label for="">isim</label>
                    <input type="text" class="form-control" name="name">
                </div> <div class="form-group">
                    <label for="">Soyisim</label>
                    <input type="text" class="form-control" name="surname">
                </div>

                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label for="">Sifre</label>
                    <input type="password" class="form-control" name="password">
                </div>
                <div class="form-group">
                    <label for="">Website</label>
                    <input type="text" class="form-control" name="website">
                </div>
                <div class="form-group">
                    <label for="">Tanıtım Metni</label>
                    <textarea  id="summernote" class="form-control" rows="4" name="contents"></textarea>
                </div>

                <div class="form-group">
                    <label >Müşteri</label>
                    <select name="customer" class="form-control">
                        <option value="">Secim Yapınız</option>
                        @foreach($customers as $customer)
                            <option value="{{ $customer->id }}">{{ $customer->name_surname }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <button class="form-control btn btn-outline-primary">Müşteri Olustur.</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('css')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                'height':300
            });
        });
    </script>
@endsection
