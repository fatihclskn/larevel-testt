@extends('admin.layouts.master')
@section('title','Müşteri güncelleme')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tüm Yazılar</h6>
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <form action="{{ route('customer.update',$customer->id) }}" method="POST" enctype="multipart/form-data" >
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="">İsim Soyisim</label>
                    <input type="text" class="form-control" name="name" value="{{ $customer->name_surname }}">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control" name="email" value="{{ $customer->email }}">
                </div>
                <div class="form-group">
                    <label >Müşteri</label>
                    <select name="customer" class="form-control">
                        <option value="">Secim Yapınız</option>
                        @foreach($company as $compan)
                            <option value="{{ $compan->id }}">{{ $compan->company_name }}</option>
                        @endforeach
                    </select>
                </div>


    </div>
                </div>

                <div class="form-group">
                    <button class="form-control btn btn-outline-primary">Bilgileri Güncelle</button>
                </div>
            </form>
        </div>
    </div>



@endsection
