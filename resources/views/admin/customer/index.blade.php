@extends('admin.layouts.master')
@section('title','Müşteriler')
@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tüm Müşteriler</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>İsim Soyisim</th>
                        <th>E-mail</th>
                        <th>Şirketler</th>
                        <th>Olusturma Tarihi</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($customers as $customer)
                        <tr>
                            <td>{{ $customer->id }}</td>
                            <td>{{ $customer->name_surname	 }}</td>
                            <td>{{ $customer->email	 }}</td>
                            <td>{{ ($customer->getCompany->company_name)}}</td>
                            <td>{{ \Carbon\Carbon::parse($customer->created_at)->format('j F, Y') }}</td>
                            <td >
                                <a href="#" class="btn btn-outline-success form-control mb-1"><i class="fa fa-eye"></i></a>
                                <a href="{{ route('customer.edit',$customer->id) }}"   class="btn btn-outline-secondary form-control mb-1"><i class="fa fa-edit"></i></a>
                                <form action="{{ route('customer.destroy',$customer->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button href="" class="btn btn-outline-danger form-control"><i class="fa fa-times"></i></button>

                                </form>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>

            </div>
        </div>
    </div>

@endsection
