@extends('admin.layouts.master')
@section('title','Müsteri Olustur')
@section('content')


    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Yeni Müşteri Kaydı</h6>
        </div>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <form action="{{ route('customer.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="">İsim</label>
                <input type="text" class="form-control" name="name">
            </div>
                <div class="form-group">
                    <label for="">Soyisim</label>
                    <input type="text" class="form-control" name="surname">
                </div>

            <div class="form-group">
                <label for="">Email</label>
                <input type="email" class="form-control" name="email">
            </div>
                <div class="form-group">
                    <label for="">Şifre</label>
                    <input type="password" class="form-control" name="password">
                </div>

                <div class="form-group">
                    <label >Müşteri</label>
                    <select name="customer" class="form-control">
                        <option value="">Secim Yapınız</option>
                        @foreach($copmanies as $copmany)
                            <option value="{{ $copmany->id }}">{{ $copmany->company_name }}</option>
                        @endforeach
                    </select>
                </div>
            <div class="form-group">
                <button class="form-control btn btn-outline-primary">Müşteri Olustur.</button>
            </div>
            </form>
        </div>
    </div>



@endsection
