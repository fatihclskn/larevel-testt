<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use function React\Promise\all;

class AuthController extends Controller
{
    public function index(Request $request )
    {


        if ($request->isMethod('post')) {

            $this->validate(request(), [
                'email' => 'required|email',
                'password' => 'required|min:5|max:20'
            ]);
          $user = User::where('email','=',$request->email)->first();
          if($user){
              if(Hash::check($request->password,$user->password)){
                  $request->session()->regenerate();
                  return  redirect()->route('admin.dashboard');
              }
              else{
                  return 'hatali giris';
              }
          }


        }
        return view('admin.auth.login');
    }

    public function  create(Request  $request)
    {
        if ($request->isMethod('post')){
            $this->validate(request(), [
                'name' => 'required|min:2|max:50',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:5|max:20'
            ]);
            $user =User::create([
                'name' =>trim($request->name),
                'email' => trim($request->email),
                'password' => Hash::make($request->password),
            ]);
            return redirect()->route('admin.login');
        }
        return view('admin.auth.register');
    }
}
