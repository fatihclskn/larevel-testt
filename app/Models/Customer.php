<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $guarded;
    public function getCompany()
    {
        return $this->hasOne(Company::class,'id','company_id')->withDefault();
    }
}
