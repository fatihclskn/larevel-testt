<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;
use Faker;
class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        for ($i=0;$i<10;$i++){
            $customer=Customer::create([
                'name_surname'=>$faker->firstName.''.$faker->lastName,
                'email'=>$faker->email,
                'password'=>$faker->password,
                'company_id'=>rand(1,10)
            ]);
        }
    }
}
