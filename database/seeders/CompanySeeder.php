<?php

namespace Database\Seeders;

use App\Models\Company;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Faker;
use Illuminate\Support\Str;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker )
    {
         for ($i=0;$i<10;$i++){
             $company = Company::create([
                 'company_name'=>$faker->company,
                 'name_surname'=>$faker->name.''.$faker->lastName,
                 'email'=>$faker->companyEmail,
                 'password'=>$faker->password,
                 'logo'=>$faker->imageUrl('100','100'),
                 'website'=>$faker->text('10').'.com',
                 'text'=>$faker->paragraph('5'),
                 'customer_id'=>rand(1,10),
                 'company_code'=>Str::random(12)

             ]);
         }
    }
}
