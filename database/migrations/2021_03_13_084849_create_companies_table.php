<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('company_name','255');
            $table->string('name_surname','255');
            $table->string('email','255');
            $table->string('password','30');
            $table->string('logo','255')->nullable();
            $table->string('website','255');
            $table->longText('text');
            $table->unsignedBigInteger('customer_id');
            $table->string('company_code','12')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
