<?php

use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::match(['post','get'],'/login',[AuthController::class,'index'])->name('admin.login');
Route::match(['post','get'],'/',[AuthController::class,'create'])->name('admin.create');

    Route::prefix('admin')->group(function (){
        Route::get('/dashboard',[DashboardController::class,'index'])->name('admin.dashboard');
        Route::resource('/company',CompanyController::class);
        Route::resource('/customer',CustomerController::class);

    });
